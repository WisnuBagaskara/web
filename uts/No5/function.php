<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Nomor 5</title>
</head>

<body>
  <h1>Output : </h1>
</body>

</html>

<?php

function tentukan_nilai($nilai)
{
  if ($nilai >= 85 && $nilai <= 100) {
    echo "Sangat Baik";
  } elseif ($nilai >= 70 && $nilai < 85) {
    echo "Baik";
  } elseif ($nilai >= 60 && $nilai < 70) {
    echo "Cukup";
  } else {
    echo "Kurang";
  }
}
echo tentukan_nilai(98) . "<br>";
echo tentukan_nilai(76) . "<br>";
echo tentukan_nilai(67) . "<br>";
echo tentukan_nilai(43);


?>