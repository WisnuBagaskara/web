<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>No4</title>
</head>
<body>
    <h1>Output</h1>
    <h2>Looping Pertama</h2>
    <div class="loop1"></div>
    <?php
    for ($i=2;$i<=20;$i+=2){
        echo $i." - I Love PHP <br>";
    }
    ?>
    <br>
    <h2>Looping Kedua</h2>
    <div class="loop2"></div>
    <?php
    for ($i=20;$i>0;$i-=2){
        echo $i." - I Love PHP <br>";
    }
    ?>
</body>
</html>
