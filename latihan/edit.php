<?php
// include database connection file
include_once("config.php");
 
// Check if form is submitted for user update, then redirect to homepage after update
if(isset($_POST['update']))
{    
    $id = $_POST['id'];
    
    $nip = $_POST['nip'];
    $name = $_POST['name'];
    $tanggal = $_POST['tanggal'];
    $status_kehadiran = $_POST['status_kehadiran'];
        
    // update user data
    $result = mysqli_query($mysqli, "UPDATE users SET nip=$nip,name='$name',tanggal='$tanggal',status_kehadiran='$status_kehadiran' WHERE id=$id");
    
    // Redirect to homepage to display updated user in l
}
?>
<?php
// Display selected user data based on id
// Getting id from url
$id = $_GET['id'];
 
// Fetech user data based on id
$result = mysqli_query($mysqli, "SELECT * FROM users WHERE id=$id");
 
while($user_data = mysqli_fetch_array($result))
{
    $nip = $user_data['nip'];
    $name = $user_data['name'];
    $tanggal = $user_data['tanggal'];
    $status_kehadiran = $user_data['status_kehadiran'];
}
?>
<html>
<head>    
    <title>Edit User Data</title>
</head>
 
<body>
    <a href="index.php">Home</a>
    <br/><br/>
    
    <form name="update_user" method="post" action="edit.php">
        <table border="0">
            <tr> 
                <td>NIP</td>
                <td><input type="text" nip="name" value=<?php echo $nip;?>></td>
            </tr>
            <tr> 
                <td>Name</td>
                <td><input type="text" name="name" value=<?php echo $name;?>></td>
            </tr>
            <tr> 
                <td>Tanggal</td>
                <td><input type="text" name="tanggal" value=<?php echo $tanggal;?>></td>
            </tr>
            <tr> 
                <td>Status Kehadiran</td>
                <td><input type="text" name="status_kehadiran" value=<?php echo $status_kehadiran;?>></td>
            </tr>
            <tr>
                <td><input type="hidden" name="id" value=<?php echo $_GET['id'];?>></td>
                <td><input type="submit" name="update" value="Update"></td>
            </tr>
        </table>
    </form>
</body>
</html>

