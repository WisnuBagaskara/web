<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function index()
	{
		$this->load->view('halaman_utama');
	}

	public function halaman_utama()
    {
        $this->load->view('halaman_utama');
    }
	public function tentang_kami()
    {
        $this->load->view('tentang_kami');
    }
	public function produk_kami()
    {
        $this->load->view('produk_kami');
    }
	public function toko_kami()
    {
        $this->load->view('toko_kami');
    }
	public function pemilik()
    {
        $this->load->view('pemilik');
    }
}
