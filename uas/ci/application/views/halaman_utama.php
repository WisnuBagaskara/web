<!DOCTYPE html>
<<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>CigarCompany</title>
        <link rel="icon" type="image/x-icon" href="<?=base_url()?>assets/assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="<?=base_url()?>https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="<?=base_url()?>https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />
        <link href="<?=base_url()?>https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet" />
    </head>
    <body>
        <header>
        <h1 class="site-heading text-center text-faded d-none d-lg-block">
                <span class="site-heading-upper text-primary mb-3">CIGARILLOS</span>
                <span class="site-heading-lower">CIGAR COMPANY</span>
            </h1>
        </header>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
            <div class="container">
                <a class="navbar-brand text-uppercase fw-bold d-lg-none" href="<?=base_url()?>assets/index.html">Start Bootstrap</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="<?=base_url()?>index.php/home/halaman_utama">Home</a></li>
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="<?=base_url()?>index.php/tentang/tentang_kami">About</a></li>
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="<?=base_url()?>index.php/produk/produk_kami">Products</a></li>
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="<?=base_url()?>index.php/toko/toko_kami">Store</a></li>
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="<?=base_url()?>index.php/owner/pemilik">Owner</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <section class="page-section clearfix">
            <div class="container">
                <div class="intro">
                    <img class="intro-img img-fluid mb-1 mb-lg-0 rounded" src="<?=base_url()?>assets/assets/img/cigar.jpeg" alt="..." />
                    <div class="intro-text left-0 text-center bg-faded p-5 rounded">
                        <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">Premium Tobacco</span>
                            <span class="section-heading-lower">Premium Cigar</span>
                        </h2>
                        <p class="mb-3">Cerutu memberikan kesan eksklusif, mewah, dan unggul. Kenikmatan cerutu ada dalam rasanya, sebuah sensasi yang khas yang hanya bisa didapatkan dengan etiket dan penilaian tertentu. Pecinta cerutu menciptakan harmoni antara momen dalam kehidupan mereka dengan pikiran mereka. Oleh karena itu dibutuhkan perjalanan panjang dan rasa syukur yang tinggi untuk menikmatinya.

Dengan filosofi ini, disini, di CIGARILLOS, kami mendedikasikan pekerjaan kami melakukan yang terbaik dalam pembuatan cerutu terbaik. Untuk mendapatkan kualitas rasa terbaik, pembuatan setiap cerutu dilakukan melalui proses yang panjang dengan perlakuan dan kontrol khusus.</p>
                        <div class="intro-button mx-auto"><a class="btn btn-primary btn-xl" href="<?=base_url()?>index.php/toko/toko_kami">Visit Us Today!</a></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section cta">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 mx-auto">
                        <div class="cta-inner bg-faded text-center rounded">
                            <h2 class="section-heading mb-4">
                                <span class="section-heading-upper">Our Promise</span>
                                <span class="section-heading-lower">To You</span>
                            </h2>
                            <p class="mb-0">Ketika Anda masuk ke toko kami untuk memulai hari Anda, kami berdedikasi untuk menyediakan Anda dengan layanan yang ramah, suasana yang ramah, dan di atas segalanya, produk unggulan yang dibuat dengan bahan-bahan berkualitas tinggi. Jika Anda tidak puas, beri tahu kami dan kami akan melakukan apa pun yang kami bisa untuk memperbaikinya!</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer class="footer text-faded text-center py-5">
            <div class="container"><p class="m-0 small">CIGARILLOS Cigar Company<br>Copyright &copy; Wisnu Bagaskara 2022</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="<?=base_url()?>https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="<?=base_url()?>assets/js/scripts.js"></script>
    </body>
</html>
