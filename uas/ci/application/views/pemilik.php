<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>CigarCompany</title>
        <link rel="icon" type="image/x-icon" href="<?=base_url()?>assets/assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="<?=base_url()?>https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="<?=base_url()?>https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" />
        <link href="<?=base_url()?>https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?=base_url()?>assets/css/style.css" rel="stylesheet" />
    </head>
    <body>
        <header>
        <h1 class="site-heading text-center text-faded d-none d-lg-block">
                <span class="site-heading-upper text-primary mb-3">CIGARILLOS</span>
                <span class="site-heading-lower">CIGAR COMPANY</span>
            </h1>
        </header>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
            <div class="container">
                <a class="navbar-brand text-uppercase fw-bold d-lg-none" href="<?=base_url()?>Start Bootstrap</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="<?=base_url()?>index.php/home/halaman_utama">Home</a></li>
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="<?=base_url()?>index.php/tentang/tentang_kami">About</a></li>
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="<?=base_url()?>index.php/produk/produk_kami">Products</a></li>
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="<?=base_url()?>index.php/toko/toko_kami">Store</a></li>
                        <li class="nav-item px-lg-4"><a class="nav-link text-uppercase" href="<?=base_url()?>index.php/owner/pemilik">Owner</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <section class="page-section">
            <div class="container">
                <div class="product-item">
                    <div class="product-item-title d-flex">
                        <div class="bg-faded p-5 d-flex me-auto rounded">
                            <h2 class="section-heading mb-0">
                                <span class="section-heading-upper">Chief Executive Officer</span>
                                <span class="section-heading-lower">Mr. Wisnu Bagaskara</span>
                            </h2>
                        </div>
                    </div>
                    <img class="product-item-img mx-auto d-flex rounded img-fluid mb-3 mb-lg-0" src="<?=base_url()?>assets/assets/img/wisnu.jpeg" alt="..." />
                    <div class="product-item-description d-flex ms-auto">
                        <div class="bg-faded p-5 rounded"><p class="mb-0">Tidak mau melihat orang kelas atas saja yang bisa menikmati rokok cerutu, Wisnu Bagaskara memproduksi langsung rokok tersebut dengan harga murah yang diberi nama GatotKaca Cigar. Hal ini sengaja dilakukan supaya masyarakat kelas menengah ke bawah bisa menikmati cerutu.
                        <br> "Orang biasa harus bisa nyicipin cigar (ceurutu). Jadi nggak hanya di kalangan atas aja, ya kan. Emang sigar itu emang rootsnya dari orang-orang kalangan atas. Menurut gua sigar bisa dinikmatin siapa aja, kalangan manapun aja," .</p></div>
                    </div>
                </div>
            </div>
        </section>
        <footer class="footer text-faded text-center py-5">
            <div class="container"><p class="m-0 small">CIGARILLOS Cigar Company<br>Copyright &copy; Wisnu Bagaskara 2022</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="<?=base_url()?>https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="<?=base_url()?>assets/js/scripts.js"></script>
    </body>
</html>
